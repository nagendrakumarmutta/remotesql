const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const router = express.Router();
const authenticateToken = require("../authenticateToken");
const {user, task, subtask, refreshtoken} = require("../models");
require("dotenv").config();

function generateAccessToken(payload){
   return jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {expiresIn:"35m"});
}


//API to register the user
router.post("/register", async(req, res)=>{
    const {username, email, password} = req.body;

    const isValidUsername = (username) => {
        const regex = /^[a-zA-Z ]{2,30}$/;
        return regex.test(username)
    }
    const isValidEmail = (email) => {
        const regex = /^[a-z0-9]+@[a-z]+\.[a-z]{2,3}$/;
        return regex.test(email)
    }
    const isValidPassword = (password) => {
        return  ((password.length >= 6) && (password.length <= 16)) 
    }
    

    if (!isValidUsername(username)) {
        res.status(400).json({msg:`USERNAME: ${username} is not valid.`})
    } else if (!isValidEmail(email)) {
        res.status(400).json({msg:`EMAIL: ${email} is not valid.`})
    } else if (!isValidPassword(password)) {
        res.status(400).json({msg:"password length should lie between 6 to 16"})
    }else{
     const hashedPassword = await bcrypt.hash(password, 10);
    const isUserFound = await user.findOne({
        where: {username: username}
    });

    if(!isUserFound){
        try{
        const userData = await user.create({
            username,
            email,
            password: hashedPassword
        })

        res.status(200).json(userData);
    }catch(err){
        console.log(err.message)
        res.status(400).json(err.message)
    } 
     
    }else{
        res.status(400).json({msg:"user already exists"})
    }
    
    }
});



//API to login
router.post("/login", async(req, res)=> {
    const {username, password} = req.body;
    
    const isUserFound = await user.findOne({
        where: {username: username}
    });

    if(isUserFound){
        const isPasswordMatched = await bcrypt.compare(password, isUserFound.password);
        
        if(isPasswordMatched){
           const payload = {
            username,
           }
           const jwtToken = generateAccessToken(payload);
           const refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET);

           await refreshtoken.create({
            username,
            token: refreshToken,
        })

        res.status(200).json({jwtToken, refreshToken})

        }else{
            res.status(400).json({msg:`invalid password`})
        }
    }else{
        res.status(400).json({msg:"invalid user"})
    }
});



// API to update the password
router.patch("/resetPassword", authenticateToken, async(req, res) => {
     const {username, password, newPassword} = req.body;
     const isUserFound = await user.findOne({
        where:{username,}
     })

     if(isUserFound){
        const isPasswordMatched = await bcrypt.compare(password, isUserFound.password);

        if(isPasswordMatched){
              const newHashedPassword = await bcrypt.hash(newPassword, 10);

              await user.update({
                password:newHashedPassword,
              },
              {
                where:{username,}
              });

              res.status(200).json({msg:"password updated successfully"});
        }else{
            res.status(400).json({msg:"invalid password"})
        }
     }else{
        res.status(400).json({msg:`invalid user`})
     }
});



// API to create accessToken
router.post("/token", async(req, res)=>{
    const refreshToken = req.body.token;
    if(refreshToken==null){
        return res.status(401).json({msg:"required token"})
    }
   const token = await refreshtoken.findAll(
    {
    where:{token:refreshToken,}
   });

   if(!(token.length === 0)){
     jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, payload)=>{
        if(!err){
           const accessToken = generateAccessToken({username:payload.username});
           res.status(200).json({accessToken,})
        }else{
            res.status(400).json({msg:"invalid refresh token"})
        }
     })
   } else{
    res.status(400).json({msg:"token not found in database"})
   }
});



// API to logout
router.delete('/logout', async(req,res) => {
       refreshtoken.destroy({
        where:{},
        truncate:true,
       }).then(()=>{
        res.status(200).json("all the rows deleted successfully")
       }).catch(err =>{
        res.status(500).json({msg:err.message})
       })
});



// API to get all the users
router.get('/users',authenticateToken, async (req,res) => {
    try {
        const usersData = await user.findAll()
        res.status(200).json(usersData)
    } catch (error) {
        res.status(500).json(error.message)
    }
});




// API to delete the specific user
router.delete('/users/delete/:id', authenticateToken, async(req, res)=>{
    const isUserFound = await user.findOne({
        where:{id:req.params.id}
    });
    console.log(isUserFound)
    if(isUserFound){
        user.destroy({
            where:{id:req.params.id,},
        }).then(()=>{
            res.status(200).json(`user with ${req.params.id} deleted successfully`)
        }).catch(err =>{
            res.status(400).json(err.message)
        })
    }else{
        res.status(404).json(`user with ${req.params.id} not found in the database`)
    }
})



//API to update the specific user
router.patch('/user/update/:id', authenticateToken, async(req, res)=>{
   
    const isUserFound = await user.findOne({
        where:{id:req.params.id},
    });

    if(isUserFound){
          const isValidUser = isUserFound.username === req.username;
          if(isValidUser){
             await user.update({username:req.body.username, email:req.body.email},{
                where:{id:req.params.id},
             });
             res.status(200).json("user updated successfully");
          
          }else{
            res.status(400).json({msg:`one user does not have access to another user data`})
          }
    }else{
        res.status(404).json({msg:`user with ${id} not found in the database`})
    }
});



//API to create a new task 
router.post("/users/task", authenticateToken, async(req, res)=>{
    const isTaskFound = await task.findAll({where:{title:req.body.title}});
    
    const title = req.body.title;
    if(isTaskFound.length !== 0){
        res.status(400).json("task already exists");
    }else if(!(title.length >=10 && title.length <=250)){
        res.status(400).json("title length should between 10 to 250 characters")
    }else{
        const newTask = await task.create({
            title,
            user_id:req.body.userId
        });
        res.status(200).json("task created successfully")
    }
});


//API to create a new sub task
router.post("/users/subtasks", authenticateToken, async(req, res)=>{
    const isTaskFound = await subtask.findAll({where:{title:req.body.title}});
    
    const title = req.body.title;
    if(isTaskFound.length !== 0){
        res.status(400).json("task already exists");
    }else if(!(title.length >=10 && title.length <=250)){
        res.status(400).json("title length should between 10 to 250 characters")
    }else{
        try{
        await subtask.create({
            title,
            task_id:req.body.task_id,
        });}
        catch(err){
            res.status(400).json(err.message)
        }
        res.status(200).json("subtask created successfully")
    }
});


//API to update a task
router.put('/users/task/:task_id', authenticateToken, async (req,res) => {
        const userData = await user.findOne({where : {username : req.username}});
        const taskData = await task.findOne({where: {user_id: userData.id,task_id: req.params.task_id}})
        
        if (taskData) {
            await task.update({title: req.body.title,user_id:req.body.user_id}, {
                where: {
                    task_id: req.params.task_id
                }
            })
            res.status(200).json(`Task with ID: ${req.params.task_id} is updated sucessfully`)
        } else {
            res.status(401).json(`${req.username} is not allowed to update the task with ID: ${req.params.task_id}`)
        }
   
}); 




module.exports = router;
