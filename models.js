const sequelize = require("./dbConnection");
const {DataTypes} = require("sequelize");

const user = sequelize.define('user',{
    id: {
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
        type:DataTypes.INTEGER
    },
    username: {
        type: DataTypes.STRING,
        length: 255,
        allowNull: false,
        unique:true,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        length: 255,
    },
    password: {
        type: DataTypes.STRING,
        length: 255,
        allowNull: false
    }
},{
    timestamps: false
});

const task  = sequelize.define('task', {
    task_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    title: {
        type: DataTypes.STRING,
        length: 255,
        allowNull: false
    },
    user_id:{
        type:DataTypes.INTEGER,
        allowNull:false,
        refrernces:{
          model:{tableName:'users'},
          key:'id'
        },
        onUpdate:'cascade',
        onDelete:'cascade'

      }

},{
    timestamps: false
});



const subtask = sequelize.define('subtask', {
    subtask_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    title: {
        type: DataTypes.STRING,
        length: 255,
        allowNull: false
    },
    task_id:{
        type:DataTypes.INTEGER,
        allowNull:false,
        refrernces:{
          model:{tableName:'task'},
          key:'id'
        },
        onUpdate:'cascade',
        onDelete:'cascade'

      }

},{
    timestamps: false
});

const refreshtoken = sequelize.define('refreshtoken',{
    id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true
    },
    username: {
        type: DataTypes.STRING,
        length: 255,
        allowNull: false,
    },
    token: {
        type: DataTypes.STRING,
        length: 255,
        allowNull: false,
        unique: true
    }
},
{
    timestamps:false
});



sequelize.sync();

module.exports = {user, task, subtask, refreshtoken};
